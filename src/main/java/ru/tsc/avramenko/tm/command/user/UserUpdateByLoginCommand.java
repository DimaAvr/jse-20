package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-update-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user info by login.";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth) throw new AccessDeniedException();
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("ENTER FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUserByLogin(login, lastName, firstName, middleName, email);
    }

}